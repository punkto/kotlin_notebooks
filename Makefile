#TODO: do this with sh functions??

CONDA_ENV_NAME=kotlin_basics


conda_setup: ## create conda environment
	conda env create --file=conda-env.yml

conda_remove_environment:  ## removes envi from system. make conda_activate before
	conda remove --name ${CONDA_ENV_NAME} --all

conda_activate: ## activates conda environment for this code, must be setup before
	echo "use: conda activate ${CONDA_ENV_NAME}"

conda_deactivate: ## dectivates conda environment
	echo "use: conda deactivate"


start_jupyter: ## starts jupyter notebook. make conda_activate before
	jupyter-notebook


help: ## show this help.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'
